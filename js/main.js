let methodData = [
  {
    supportedMethods: ['visa'],
  }
];

let details = {
  displayItems: [
    {
      label: "Completely legal item",
      amount: { currency: "USD", value: '0.24' }
    }
  ],
  total: {
    label: "Total",
    amount: { currency: "USD", value: '1.20'}
  },
  shippingOptions: [
    {
      id: "standard-shipping",
      label: "Standard internet shipping",
      amount: {currency: "USD", value: '1.44'},
      selected: true
    }
  ]
};

let options = {
  requestShipping: !true,
};

var request = new PaymentRequest(methodData, details, options);

window.addEventListener("DOMContentLoaded", () => {
  document.querySelector('button').addEventListener('click', () => {
    request.show()
  });
})